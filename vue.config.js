module.exports = {
  publicPath: process.env.NODE_ENV === 'prod' ? '/dist/': '/',
  pluginOptions: {
    foo: {
      test: /\.scss$/,
      loader: 'pug-plain-loader',
      use: [
        'vue-style-loader',
        'css-loader',
        'sass-loader'
      ]
    },
    i18n: {
      locale: 'es',
      fallbackLocale: 'es',
      localeDir: 'locales',
      enableInSFC: true
    }
  },
  pwa: {
    name: 'gws-dashboard',
    short_name:'gws-dashboard',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: 'InjectManifest',
    display:'standalone',
    start_url: ".",
    workboxOptions: {
      swSrc: 'src/service-worker.js'
    }
  }
}
