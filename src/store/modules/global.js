import axios from '@/api/'
import router from '@/router'

const state = {
  sidebarDisplay: true,
  loading: false,
  userLoged: null,
  authorization: null,
  token: null,
  windowWidth: window.innerWidth,
  currentRute:'/'
};

const getters = {
  loadingState: state => state.loading,
  getToken: state => state.token,
  getAuthorization: state => state.authorization,
  getUserLoged: state => state.userLoged,
  sidebarDisplay: state => state.sidebarDisplay,
  windowWidth: state => state.windowWidth,
  getCurrentRute: state => state.currentRute
};

const actions = {
  toggleSidebarDisplay({ commit }, display) {
    commit('setSidebarDisplay',display);
  },
  login({ commit },user) {
    commit('authUser', user)
  },
  logout({ commit }) {
    commit('logoutUser')
  },
  upadteWindowWidth({ commit }) {
    commit('setWindowWidth')
  },
  setLoadingState({ commit }, mode) {
    commit('loadingState',mode)
  },
  setCurrentRute({ commit }) {
    commit('currentRute')
  }
};

const mutations = {
  authUser(state, user) {
    state.userLoged = user.data;
    state.authorization = user.Authorization
    state.token = user.token;
    axios.defaults.headers.common['Authorization'] = user.Authorization;
  },
  logoutUser(state) {
    state.userLoged = ''
    state.authorization = ''
    state.token = ''
    delete axios.defaults.headers.common['Authorization'];
  },
  setSidebarDisplay: (state, display=null) => {
    state.sidebarDisplay = display;
    const htmlClassName = 'sidebar-is-display'
    if (state.sidebarDisplay) {
      document.documentElement.classList.add(htmlClassName)
    } else {
      document.documentElement.classList.remove(htmlClassName)
    }
  },
  setWindowWidth(state) {
    state.windowWidth = window.innerWidth;
  },
  loadingState(state,mode) {
    state.loading = mode;
  },
  currentRute(state) {
    state.currentRute = router.currentRoute.name;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};