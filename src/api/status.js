import axios_config from './index'
var status = {};
status.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/status`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
status.edit = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/status/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
status.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/status/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
status.delete_logic = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/status/${id}/logic`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
status.create = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/status`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
status.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/status/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default status