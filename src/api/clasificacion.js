import axios_config from './index'
var clasificacion = {};
clasificacion.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/clasificacion`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
clasificacion.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/clasificacion/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
clasificacion.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/clasificacion`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
clasificacion.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/clasificacion/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
clasificacion.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/clasificacion/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

clasificacion.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/clasificacion/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
clasificacion.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/clasificacion/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default clasificacion