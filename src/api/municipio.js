import axios_config from './index'
var municipio = {};
municipio.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/municipio`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
municipio.edit = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/municipio/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
municipio.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/municipio/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
municipio.delete_logic = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/municipio/${id}/logic`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
municipio.create = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/municipio`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
municipio.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/municipio/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default municipio