import axios_config from './index'
var clase = {};
clase.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/clase`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
clase.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/clase/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
clase.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/clase`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
clase.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/clase/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
clase.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/clase/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

clase.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/clase/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
clase.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/clase/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default clase