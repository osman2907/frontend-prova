import axios_config from './index'
var producto = {};
producto.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/producto`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
producto.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/producto/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
producto.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/producto`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
producto.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/producto/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
producto.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/producto/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
producto.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/producto/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
producto.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/producto/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default producto