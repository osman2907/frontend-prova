import axios_config from './index'
var impresora = {};
impresora.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/impresora`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
impresora.edit = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/impresora/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
impresora.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/impresora/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
impresora.delete_logic = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/impresora/${id}/logic`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
impresora.create = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/impresora`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
impresora.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/impresora/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default impresora