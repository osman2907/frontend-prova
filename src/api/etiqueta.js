import axios_config from './index'
var etiqueta = {};
etiqueta.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/etiqueta`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
etiqueta.edit = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/etiqueta/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
etiqueta.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/etiqueta/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
etiqueta.delete_logic = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/etiqueta/${id}/logic`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
etiqueta.create = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/etiqueta`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
etiqueta.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/etiqueta/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default etiqueta