import axios_config from './index'
var privilegio = {};
privilegio.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/privilegio`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
privilegio.edit = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/privilegio/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

privilegio.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/privilegio/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
privilegio.delete_logic = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/privilegio/${id}/logic`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
privilegio.create = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/privilegio`, data)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};
privilegio.referencias = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/privilegio/${id}/edit`)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};

export default privilegio