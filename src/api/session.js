import axios_config from './index'
import store from '@/store'
import router from '@/router'
import { ToastProgrammatic as Toast } from 'buefy'


var session = {};
session.logIn = (user)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/authenticate`,user)
        .then(function (response) {
            resolve (response)
        })
        .catch(function (error) {
            reject (error)
        })
    })
};
session.logOut = ()=> {
    store.dispatch('logout').then(()=>{
        router.replace('/login'),error => console.error(error)
    })
};

session.closeSession = ()=> {
    Toast.open({
        duration: 2000,
        message: `Seesion end`,
        position: 'is-bottom',
        type: 'is-danger'
    })
    store.dispatch('logout').then(()=>{
        router.replace('/login'),error => console.error(error)
    })
};
export default session