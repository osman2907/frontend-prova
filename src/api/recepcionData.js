import axios_config from './index'
var recepciondata = {};
recepciondata.list = (recepcionId)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/recepciondata/${recepcionId}`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
recepciondata.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/recepciondata/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
recepciondata.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/recepciondata`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
recepciondata.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/recepciondata/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
recepciondata.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/recepciondata/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
recepciondata.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/recepciondata/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
recepciondata.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/recepciondata/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default recepciondata