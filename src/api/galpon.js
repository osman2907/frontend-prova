import axios_config from './index'
var galpon = {};
galpon.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/galpon`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
galpon.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/galpon/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
galpon.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/galpon`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
galpon.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/galpon/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
galpon.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/galpon/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
galpon.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/galpon/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
galpon.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/galpon/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default galpon