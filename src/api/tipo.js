import axios_config from './index'
var tipo = {};
tipo.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/tipo`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
tipo.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/tipo/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
tipo.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/tipo`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
tipo.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/tipo/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
tipo.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/tipo/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

tipo.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/tipo/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
tipo.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/tipo/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default tipo