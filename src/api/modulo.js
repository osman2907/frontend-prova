import axios_config from './index'
var modulo = {};
modulo.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/modulo`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
modulo.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/modulo/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
modulo.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/modulo`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
modulo.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/modulo/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
modulo.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/modulo/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
modulo.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/modulo/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
modulo.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/modulo/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default modulo