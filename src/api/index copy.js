import axios from 'axios'
import store from '@/store'
import router from '@/router'
import { SnackbarProgrammatic as Snackbar } from 'buefy'
import { ToastProgrammatic as Toast } from 'buefy'
import app from '../main'; // import the instance

const AUTH_TOKEN = store.getters.authorization;
const axios_config = axios.create({
    baseURL: process.env.VUE_APP_URLBASE,
    //baseURL: window.location.protocol +"//"+ window.location.hostname + ":" + process.env.VUE_APP_PORT,
    timeout: 30000,
    headers:{'Accept-Language': process.env.VUE_APP_I18N_LOCALE}
});

axios_config.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// // // Add a request interceptor
axios_config.interceptors.request.use(
    config => {
        app.$Progress.start()
        if (AUTH_TOKEN) {
            config.headers['Authorization'] = AUTH_TOKEN;
        }
        return config;
    },
    error => {
        app.$Progress.fail()
        if(error.response && error.response.status == 403 ){
            Snackbar.open({
                position: 'is-top',
                type: 'is-warning',
                message:`${error.response.status} - ${error.response.data.message}`,
            })
            router.push({name:'403',params: {error: error.response}}),error => console.error(error)
            return Promise.reject(error);
        }else if(error.response && error.response.status == 401 ){
            if(error.response.data.error == "token_expired"){
                store.dispatch('logout').then(()=>{ router.push('/login'),error => console.error(error)})
            }else{
                return Promise.reject(error);
            }
        }else if(error.response && error.response.status == 400 ){
            let message =  `<span class="is-family-code has-text-left">
                                <h4 class="has-text-weight-bold">ERROR: <b>${error.response.status}</b></h4>
                                </br>
                                ${error.response.data.message}
                            </span>`
            Toast.open({
                duration: 5000,
                message: message,
                position: 'is-top-right',
                type: 'is-danger'
            })
            return Promise.reject(error.response.data);
           
        }else if(error == 'Error: Network Error' ){
            router.push('/login'),error => console.error(error)
        }else{
            return Promise.reject(error);
        }
    }
);


axios_config.interceptors.response.use(
    config => {
        app.$Progress.finish()
        if (AUTH_TOKEN) {
            config.headers['Authorization'] = AUTH_TOKEN;
        }
        
        if(config.config.method == "post" || config.config.method == "put" && config.status == 200){
            console.log('response',config)
            let message =  `<span class="is-family-code has-text-left">
                                ${config.data.message}
                            </span>`
            Toast.open({
                duration: 5000,
                message: message,
                position: 'is-top-right',
                type: 'is-success'
            })
        }
        return config;
    },
    error => {
        app.$Progress.fail()
        if(error.response && error.response.status == 403 ){
            Snackbar.open({
                position: 'is-top',
                type: 'is-warning',
                message:`${error.response.status} - ${error.response.data.message}`,
            })
            return Promise.reject(error);
        }else if(error.response && error.response.status == 401 ){
            if(error.response.data.error == "token_expired"){
                store.dispatch('logout').then(()=>{ router.push('/login'),error => console.error(error)})
            }else{
                return Promise.reject(error);
            }
        }else if(error.response && error.response.status == 400 ){
            console.warn(error.response)
            let message =  `<span class="is-family-code has-text-left">
                                <h4 class="has-text-weight-bold">ERROR: <b>${error.response.status}</b></h4>
                                </br>
                                ${error.response.data.message}
                            </span>`
            Toast.open({
                duration: 5000,
                message: message,
                position: 'is-top-right',
                type: 'is-danger'
            })
            return Promise.reject(error.response.data);
           
        }else if(error == 'Error: Network Error' ){
            router.push('/networkerror'),error => console.error(error)
        }else{
            return Promise.reject(error);
        }
    }
);

export default axios_config