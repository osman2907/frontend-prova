import axios_config from './index'
var cliente = {};
cliente.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/cliente`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
cliente.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/cliente/${id}/edit`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
cliente.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/cliente/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
cliente.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/cliente/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
cliente.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/cliente/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
cliente.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/cliente/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
cliente.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/cliente`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
cliente.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/cliente/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default cliente