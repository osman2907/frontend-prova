import axios_config from './index'
var moment = require('moment');
var recepcion = {};
recepcion.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/recepcion`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
recepcion.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/recepcion/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
recepcion.store = (data)=> {
    return new Promise((resolve, reject)=> {
        data.fecha=moment(data.fecha).format('YYYY-MM-DD');
        axios_config.post(`/recepcion`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
recepcion.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/recepcion/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
recepcion.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        data.fecha=moment(data.fecha).format('YYYY-MM-DD');
        axios_config.put(`/recepcion/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
recepcion.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/recepcion/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
recepcion.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/recepcion/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default recepcion