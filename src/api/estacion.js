import axios_config from './index'
var estacion = {};
estacion.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/estacion`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
estacion.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/estacion/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
estacion.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/estacion`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
estacion.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/estacion/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
estacion.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/estacion/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
estacion.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/estacion/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
estacion.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/estacion/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default estacion