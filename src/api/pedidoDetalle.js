import axios_config from './index'
var pedidoDetalle = {};
pedidoDetalle.list = (productoId) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pedidoDetalle/${productoId}`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pedidoDetalle.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pedidoDetalle/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
pedidoDetalle.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/pedidoDetalle`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pedidoDetalle.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pedidoDetalle/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
pedidoDetalle.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/pedidoDetalle/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

pedidoDetalle.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/pedidoDetalle/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pedidoDetalle.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/pedidoDetalle/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

export default pedidoDetalle