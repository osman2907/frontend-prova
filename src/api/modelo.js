import axios_config from './index'
var modelo = {};
modelo.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/modelo`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
modelo.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/modelo/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
modelo.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/modelo`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
modelo.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/modelo/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
modelo.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/modelo/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
modelo.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/modelo/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
modelo.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/modelo/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default modelo