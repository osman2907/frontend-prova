import axios_config from './index'
var estado = {};
estado.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/estado`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
estado.edit = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/estado/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

estado.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/estado/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
estado.delete_logic = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/estado/${id}/logic`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
estado.create = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/estado`, data)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};
estado.referencias = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/estado/${id}/edit`)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};

export default estado