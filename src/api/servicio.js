import axios_config from './index'
var servicio = {};
servicio.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/servicio`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
servicio.edit = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/servicio/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
servicio.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/servicio/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
servicio.delete_logic = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/servicio/${id}/logic`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
servicio.create = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/servicio`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
servicio.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/servicio/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default servicio