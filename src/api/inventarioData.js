import axios_config from './index'
var inventarioData = {};
inventarioData.list = (productoId) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/inventarioData/${productoId}`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
inventarioData.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/inventarioData/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
inventarioData.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/inventarioData`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
inventarioData.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/inventarioData/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
inventarioData.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/inventarioData/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

inventarioData.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/inventarioData/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
inventarioData.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/inventarioData/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

export default inventarioData