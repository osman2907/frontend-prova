import axios_config from './index'
var equipo = {};
equipo.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/equipo`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
equipo.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/equipo/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
equipo.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/equipo`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
equipo.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/equipo/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
equipo.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/equipo/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

equipo.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/equipo/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
equipo.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/equipo/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default equipo