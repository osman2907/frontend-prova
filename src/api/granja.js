import axios_config from './index'
var granja = {};
granja.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/granja`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
granja.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/granja/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
granja.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/granja`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
granja.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/granja/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
granja.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/granja/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
granja.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/granja/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
granja.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/granja/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};


export default granja