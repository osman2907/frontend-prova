import axios_config from './index'
var proveedor = {};
proveedor.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/proveedor`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
proveedor.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/proveedor/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
proveedor.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/proveedor`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
proveedor.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/proveedor/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
proveedor.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/proveedor/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
proveedor.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/proveedor/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
proveedor.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/proveedor/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};


export default proveedor