import axios_config from './index'
var condicion = {};
condicion.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/condicion`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
condicion.edit = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/condicion/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

condicion.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/condicion/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
condicion.delete_logic = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/condicion/${id}/logic`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
condicion.create = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/condicion`, data)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};
condicion.referencias = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/condicion/${id}/edit`)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};

export default condicion