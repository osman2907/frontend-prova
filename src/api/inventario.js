import axios_config from './index'
var inventario = {};
inventario.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/inventario`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
inventario.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/inventario/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
inventario.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/inventario`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
inventario.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/inventario/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
inventario.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/inventario/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

inventario.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/inventario/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
inventario.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/inventario/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default inventario