import axios_config from './index'
var productoEmpaque = {};
productoEmpaque.list = (productoId)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/productoempaque/${productoId}`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
productoEmpaque.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/productoempaque/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
productoEmpaque.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/productoempaque`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
productoEmpaque.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/productoempaque/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
productoEmpaque.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/productoempaque/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
productoEmpaque.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/productoempaque/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
productoEmpaque.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/productoempaque/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default productoEmpaque