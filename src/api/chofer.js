import axios_config from './index'
var chofer = {};
chofer.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/chofer`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
chofer.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/chofer/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
chofer.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/chofer`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
chofer.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/chofer/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
chofer.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/chofer/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
chofer.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/chofer/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
chofer.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/chofer/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default chofer