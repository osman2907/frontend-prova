import axios_config from './index'
var estatus = {};
estatus.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/estatus`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
estatus.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/estatus/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
estatus.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/estatus`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
estatus.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/estatus/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
estatus.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/estatus/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
estatus.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/estatus/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
estatus.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/estatus/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default estatus