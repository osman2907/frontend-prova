import axios_config from './index'
var ciudad = {};
ciudad.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/ciudad`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
ciudad.edit = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/ciudad/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

ciudad.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/ciudad/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
ciudad.delete_logic = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/ciudad/${id}/logic`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
ciudad.create = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/ciudad`, data)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};
ciudad.referencias = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/ciudad/${id}/edit`)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};

export default ciudad