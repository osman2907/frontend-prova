import axios_config from './index'
var observacion = {};
observacion.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/observacion`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
observacion.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/observacion/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
observacion.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/observacion`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
observacion.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/observacion/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
observacion.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/observacion/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

observacion.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/observacion/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
observacion.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/observacion/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default observacion