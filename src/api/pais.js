import axios_config from './index'
var pais = {};
pais.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pais`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pais.edit = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/pais/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

pais.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/pais/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pais.delete_logic = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/pais/${id}/logic`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pais.create = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/pais`, data)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};
pais.referencias = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pais/${id}/edit`)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};

export default pais