import axios_config from './index'
var empresa = {};
empresa.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/empresa`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
empresa.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/empresa/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
empresa.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/empresa`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
empresa.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/empresa/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
empresa.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/empresa/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
empresa.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/empresa/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
empresa.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/empresa/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};


export default empresa