import axios from 'axios'
import store from '@/store'
import router from '@/router'
// import { SnackbarProgrammatic as Snackbar } from 'buefy'
// import { ToastProgrammatic as Toast } from 'buefy'
import app from '../main'; // import the instance

const AUTH_TOKEN = store.getters.authorization;

const axios_config = axios.create({
    baseURL: process.env.VUE_APP_URLBASE,
    //baseURL: window.location.protocol +"//"+ window.location.hostname + ":" + process.env.VUE_APP_PORT,
    timeout: 40000,
    headers:{'Accept-Language': process.env.VUE_APP_I18N_LOCALE}
});

axios_config.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// // // Add a request interceptor
axios_config.interceptors.request.use(
    config => {
        app.$Progress.start()
        if (AUTH_TOKEN) { config.headers['Authorization'] = AUTH_TOKEN; }
        return config;
    },
    error => {
        app.$Progress.fail()
        controlApiCode(error)
        return Promise.reject(error);
    }
);

axios_config.interceptors.response.use(
    config => {
        if (AUTH_TOKEN) { config.headers['Authorization'] = AUTH_TOKEN; }
        app.$Progress.finish()
        return config;
    },
    error => {
        app.$Progress.fail()
        controlApiCode(error)
        return Promise.reject(error);
    }
);

const controlApiCode = (api)=>{
    if(api == 'Error: Network Error')
        router.push('/networkerror'),error => console.error(error)

    if(api.response.data.error == "token_expired")
        store.dispatch('logout').then(()=>{ router.push('/login'),error => console.error(error)})

    switch(api.status)
    {
      case 200: break; //todo exito
      case 401: break; // Error de autenticacion
      case 402: break; // error al realizar la operacion en base de datos
    //   case 400: break; // error inesperado
      case 422: break; // Datos existentes
      case 204: break; // Data no recuperable
      case 400: app.$toast(`Code ${api.status}. Invalid API location. Check the URL that you are using`); break;
      case 403: app.$toast(`Code ${api.status}. Invalid or missing API key. Check that your API key is present and matches your assigned key`); break;
      case 405: app.$toast(`Code ${api.status}. Invalid HTTP method. Check that the method (POST|GET|PUT|DELETE) matches what the documentation indicates`); break;
      case 500: app.$toast(`Code ${api.status}. Internal server error. Try again at a later time`); break;
      case 412: app.$toast(`Code ${api.status}. Request failed: ${api.data.message}`); break;
      case 404: app.$toast(`Code ${api.status}. URL Invalid`); break;
      default: //app.$toast(`Undefined: ${api.status}`); break;
    }
}

export default axios_config