import axios_config from './index'
var merma = {};
merma.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/merma`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
merma.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/merma/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
merma.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/merma`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
merma.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/merma/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
merma.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/merma/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

merma.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/merma/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
merma.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/merma/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default merma