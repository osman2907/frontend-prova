import axios_config from './index'
var genero = {};
genero.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/genero`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
genero.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/genero/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
genero.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/genero`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
genero.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/genero/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
genero.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/genero/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
genero.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/genero/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
genero.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/genero/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default genero