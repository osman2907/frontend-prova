import axios_config from './index'
var users = {};
users.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/users`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
export default users