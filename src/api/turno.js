import axios_config from './index'
var turno = {};
turno.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/turno`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
turno.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/turno/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
turno.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/turno`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
turno.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/turno/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
turno.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/turno/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
turno.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/turno/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
turno.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/turno/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default turno