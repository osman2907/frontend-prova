import axios_config from './index'
var tara = {};
tara.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/tara`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
tara.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/tara/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
tara.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/tara`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
tara.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/tara/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
tara.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/tara/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

tara.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/tara/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
tara.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/tara/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default tara