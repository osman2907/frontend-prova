import axios_config from './index'
var operador = {};
operador.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/operador`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
operador.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/operador/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
operador.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/operador`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
operador.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/operador/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
operador.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/operador/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

operador.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/operador/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
operador.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/operador/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default operador