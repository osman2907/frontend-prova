import axios_config from './index'
var camion = {};
camion.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/camion`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
camion.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/camion/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
camion.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/camion`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
camion.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/camion/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
camion.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/camion/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

camion.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/camion/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
camion.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/camion/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default camion