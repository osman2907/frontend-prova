import axios_config from './index'
var forma = {};
forma.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/forma`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
forma.edit = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/forma/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
forma.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/forma/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
forma.delete_logic = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/forma/${id}/logic`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
forma.create = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/forma`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};
forma.referencias = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/forma/${id}/edit`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {
            console.log(err);
            reject(err)
        })
    });
};

export default forma