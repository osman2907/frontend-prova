import axios_config from './index'
var persona = {};
persona.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/persona`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
persona.edit = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/persona/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

persona.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/persona/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
persona.delete_logic = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/persona/${id}/logic`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
persona.create = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/persona`, data)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};
persona.referencias = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/persona/${id}/edit`)
            .then((res) => { resolve(res); })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
    });
};

export default persona