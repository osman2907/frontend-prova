import axios_config from './index'
var moment = require('moment');
var pedido = {};
pedido.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pedido`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pedido.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pedido/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
pedido.store = (data) => {
    return new Promise((resolve, reject) => {
        data.fecha=moment(data.fecha).format('YYYY-MM-DD');
        axios_config.post(`/pedido`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pedido.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/pedido/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
pedido.update = (data, id) => {
    return new Promise((resolve, reject) => {
        data.fecha=moment(data.fecha).format('YYYY-MM-DD');
        axios_config.put(`/pedido/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

pedido.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/pedido/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
pedido.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/pedido/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default pedido