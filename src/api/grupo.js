import axios_config from './index'
var grupo = {};
grupo.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/grupo`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
grupo.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/grupo/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
grupo.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/grupo`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
grupo.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/grupo/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
grupo.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/grupo/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
grupo.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/grupo/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
grupo.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/grupo/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default grupo