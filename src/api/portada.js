import axios_config from './index'
var portada = {};
portada.recepciondespacho = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`inicio/portada`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

portada.graficatotal = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`inicio/portadagraficatotal`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
portada.graficadesglosado = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`inicio/portadagraficadesglosado/${id}`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
export default portada