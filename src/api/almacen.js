import axios_config from './index'
var almacen = {};
almacen.list = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/almacen`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
almacen.create = ()=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/almacen/create`)
        .then((res)=> { resolve(res.data);})
        .catch((err)=> {reject(err)})
    });
};
almacen.store = (data)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/almacen`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> { reject(err) })
    });
};
almacen.edit = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.get(`/almacen/${id}/edit`)
        .then((res)=> { resolve(res.data.referencias);})
        .catch((err)=> {reject(err)})
    });
};
almacen.update = (data,id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.put(`/almacen/${id}`, data)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};
 
almacen.recycle = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.post(`/almacen/${id}/recycle`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
}; 
almacen.destroy = (id)=> {
    return new Promise((resolve, reject)=> {
        axios_config.delete(`/almacen/${id}/destroy`)
        .then((res)=> { resolve(res);})
        .catch((err)=> {reject(err)})
    });
};

export default almacen