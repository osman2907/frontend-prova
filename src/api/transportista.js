import axios_config from './index'
var transportista = {};
transportista.list = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/transportista`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
transportista.create = () => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/transportista/create`)
            .then((res) => { resolve(res.data); })
            .catch((err) => { reject(err) })
    });
};
transportista.store = (data) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/transportista`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
transportista.edit = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.get(`/transportista/${id}/edit`)
            .then((res) => { resolve(res.data.referencias); })
            .catch((err) => { reject(err) })
    });
};
transportista.update = (data, id) => {
    return new Promise((resolve, reject) => {
        axios_config.put(`/transportista/${id}`, data)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};

transportista.recycle = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.post(`/transportista/${id}/recycle`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};
transportista.destroy = (id) => {
    return new Promise((resolve, reject) => {
        axios_config.delete(`/transportista/${id}/destroy`)
            .then((res) => { resolve(res); })
            .catch((err) => { reject(err) })
    });
};


export default transportista