import Vue from 'vue'
import Router from 'vue-router'

import adminTemplate from '@/layouts/admin'
import loginTemplate from '@/layouts/login'


let rute = (rute) => {
	return () => import(`@/views/${rute}`)
}

Vue.use(Router)

let router = new Router({
	routes: [
		{
			path: '/',
			component: adminTemplate,
			meta: {
				requiresAuth: true,
				title: 'admin',
			},
			children: [
				{
					path: '/',
					icon: 'tachometer-alt',
					name: 'home',
					component: rute('home/'),
					meta: {
						title: 'home'
					}
				},
				{
					path: '',
					icon: 'list-alt',
					name: 'orders',
					meta: { title: 'catalog parent'},
					component: rute('catalog/'),
					activeSubmenu:false,
					children: [
						{
							path: '/catalog/pedidosRecepcion',
							icon: 'list-alt',
							name: 'receptionorders',
							component: rute('catalog/pedidosRecepcion/'),
							meta: { title:'receptionorders'}
						},
						{
							path: '/catalog/pedidosDespacho',
							icon: 'list-alt',
							name: 'customerorders',
							component: rute('catalog/pedidosDespacho'),
							meta: { title:'customerorders'}
						}
					]
				},
				{
					path: '',
					icon: 'book',
					name: 'catalogs',
					meta: { title: 'catalog parent'},
					component: rute('catalog/'),
					activeSubmenu:false,
					children: [
						{
							path: '/catalog/almacenes',
							icon: 'warehouse',
							name: 'warehouses',
							component: rute('catalog/almacenes/'),
							meta: { title:'almacenes'}
						},
						{
							path: '/catalog/camiones',
							icon: 'truck',
							name: 'trucks',
							component: rute('catalog/camiones/'),
							meta: { title:'camiones'}
						},
						{
							path: '/catalog/choferes',
							icon: 'user-alt-slash',
							name: 'drivers',
							component: rute('catalog/choferes/'),
							meta: { title:'choferes'}
						},
						{
							path: '/catalog/clientes',
							icon: 'user-friends',
							name: 'customers',
							component: rute('catalog/clientes/'),
							meta: { title:'clientes'}
						},
						{
							path: '/catalog/producto',
							icon: 'boxes',
							name: 'products',
							component: rute('catalog/producto/'),
							meta: { title:'productos'}
						},
						{
							path: '/catalog/proveedor',
							icon: 'truck-loading',
							name: 'providers',
							component: rute('catalog/proveedor/'),
							meta: { title:'providers'}
						},
						{
							path: '/catalog/taras',
							icon: 'weight',
							name: 'taras',
							component: rute('catalog/taras/'),
							meta: { title:'taras'}
						}
						
					]
				},
				{
					path: '',
					icon: 'cog',
					name: 'setup',
					meta: { title: 'setup parent'},
					component: rute('setup/'),
					activeSubmenu:false,
					children: [
						{
							path: '/setup/empresa',
							icon: 'building',
							name: 'company',
							component: rute('setup/empresa/'),
							meta: { title:'company'}
						},
						{
							path: '/setup/equipos',
							icon: 'laptop-code',
							name: 'equipments',
							component: rute('setup/operadores/'),
							meta: { title:'equipments'}
						},
						{
							path: '/catalog/operadores',
							icon: 'users-cog',
							name: 'operators',
							component: rute('setup/operadores/'),
							meta: { title:'operators'}
						},
						{
							path: '/setup/roles',
							icon: 'user-tag',
							name: 'roles',
							component: rute('setup/roles/'),
							meta: { title:'roles'}
						},
						{
							path: '/setup/users',
							icon: 'users',
							name: 'users',
							component: rute('setup/users/'),
							meta: { title:'users'}
						}
					]
				}
			]
		},
		{
			path: '/',
			redirect: 'login',
			component: loginTemplate,
			children: [
				{
					path: '/login',
					name: 'login',
					component: rute('login/'),
					meta: {
						title:'login'
					}
				}
			]
		},
		{
			path: '*',
			component: rute('errors/notfound.vue'),
			meta: {
				title:'Error'
			}
		},
		{
			name: '403',
			path: '/403',
			component: rute('errors/forbidden.vue'),
			props: true,
			meta: {
				title:'Error'
			}
		}
	]
})

export default router