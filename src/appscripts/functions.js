// import { SnackbarProgrammatic as Snackbar } from 'buefy'
import { ToastProgrammatic as Toast } from 'buefy'

const Functions = {
    install(Vue) {

        //-mensajes emeergentes -- https://buefy.org/documentation/toast
        Vue.prototype.$toast = function (message = 'empty', type = 'white', position = 'top-right', duration = 3000) {
            Toast.open({ duration: duration, message: message, position: `is-${position}`, type: `is-${type}` })
        }
        
        //- json valores numericos a integer
        Vue.prototype.$jsonObjToInt = function(json){
            let toString = JSON.stringify(json)
            let resutl = JSON.parse(toString, function(k, v) {  return (typeof v === "object" || isNaN(v)) ? v : parseInt(v, 10); });
            return resutl
        }
        
        //-- recive un str devuelve un array
        Vue.prototype.$valToArray = function(str){
            var isArray = str.toString().split(',')
            var a = isArray.filter(Boolean)
            var b = a.map(e=> {return parseInt(e)})
            var c = b.sort(function(a, b) {return a - b;})
            return c
        }
    

    }
};
export default Functions;