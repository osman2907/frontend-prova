import moment from "moment"
const Filters = {
    install(Vue) {

        Vue.filter('validNull', function (value) {
            if(value==null){  return `NULL`;}
            else if(value==undefined){  return `undefined`;}
            else if(value==''){  return `Empty`;}
            else{ return `****`;}
        });

        Vue.filter('toUSD', function (value) {
            return `$${value}`;
        });

        Vue.filter('uppercase', function (value) {
            return value.toUpperCase();
        });
        Vue.filter('lowercase', function (value) {
            return value.toLowerCase();
        });
        Vue.filter('json', function (value) {
            return JSON.stringify(value);
        });
        Vue.filter('dateFormat', function (value) {
            return moment(value, 'YYYY-MM-DD').format('DD/MM/YYYY');
        });
        Vue.filter('hourFormat', function (value) {
            return moment(value, 'HH:mm:ss').format('HH:mm:ss');
        });
        Vue.filter('map', function (objects, key) {
            return objects.map(function(object) { 
                return object[key];
            });
        });
    

    }
};

export default Filters;