const Directives = {
    install(Vue) {

        Vue.directive('focus', {
            inserted: function (el) {
                el.focus()
            }
        })

        Vue.directive('rand', {
            bind(el) {
                el.style.color = "#" + Math.random().toString().slice(2, 8)
            }
        })


    }
};
export default Directives;