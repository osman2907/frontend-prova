import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import i18n from './i18n'
import axiosConfig from './api'
import Moment from 'moment'
import vueMoment from 'vue-moment'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import Functions from "./appscripts/functions"
Vue.use(Functions)

import Filters from "./appscripts/filters"
Vue.use(Filters)

import Directives from "./appscripts/directives"
Vue.use(Directives)

Vue.use(VueLodash, { name: 'custom' , lodash: lodash })

Moment.locale(i18n.locale)
Vue.use(vueMoment);

//global styles
import './assets/fontawesome/css/all.min.css'

const BuefyOptions = { 
  defaultIconPack: 'fas',
  defaultContainerElement: '#content',
  defaultFirstDayOfWeek:1,
  defaultDateFormatter: (date)=>{ return Moment(new Date(date)).format('DD-MM-YYYY')},
  defaultDateParser:(date)=>{ const m = Moment(date).format('YYYY-MM-DD'); return m.isValid() ? m.toDate() : null },
  defaultDateCreator:(date)=>{ const m = Moment(date,'YYYY-MM-DD').toDate(); return m },
  defaultMonthNames:Moment.months(),
  defaultDayNames:Moment.weekdaysShort()
}
import Buefy from 'buefy'
Vue.use(Buefy, BuefyOptions)

import './assets/scss/styles.scss'
import './assets/scss/animate.css'

//progress
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {color: '#4f8e3a',failedColor: 'red',thickness: '6px'})


//router
router.afterEach(() => {
  store.dispatch('setCurrentRute')
  if(window.innerWidth < 400){
    store.commit('setSidebarDisplay', false )
  }else{
    store.commit('setSidebarDisplay', true )
  }
})
  // var local=process.env.VUE_APP_I18N_LOCALE || 'es';
  // console.log('i18n.messages.es.nav.'+to.meta.title) 
router.beforeEach((to, from, next) => {
  axiosConfig.defaults.headers.common['Authorization'] = store.state.global.authorization;
  document.title = `GWS - ${to.meta.title}`
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.global.token) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})


Vue.config.productionTip = false

export default new Vue({
  App,
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
