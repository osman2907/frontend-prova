# GWS SISTEMA vue.js V-DASHBOARD
## Project setup
```
npm -v <= 12.14.1
@vue/cli
npm install
```
### flag npm install slow networt
```
 -timeout=99999999
```

## ENV setup file .env
```
NODE_ENV=
VUE_APP_I18N_LOCALE=
VUE_APP_I18N_FALLBACK_LOCALE=
VUE_APP_URLBASE=
VUE_APP_PORT=
```
### Compiles and hot-reloads for development
```
npm run dev
```
### Compiles and minifies for production
```
npm run prod
```
### Lints and fixes files
```
npm lint
```
### Build and minifies for production 
```
npm build
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
